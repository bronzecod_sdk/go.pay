package pay

import (
	"fmt"
	"net/http"

	uuid "github.com/satori/go.uuid"
)

func (c *connection) CustomerSourceAdd(customer uuid.UUID, method string) (Source, error) {
	var res Source
	d := marshal(map[string]interface{}{
		"customer": customer,
		"token":    method,
	})
	err := c.processRequest(http.MethodPut, fmt.Sprintf("%v://%v/payapi/customer/source/add", c.proto, c.base), _json, nil, d, responseJSON, &res)
	return res, err
}

func (c *connection) CustomerSourceRemove(id uuid.UUID) error {
	err := c.processRequest(http.MethodDelete, fmt.Sprintf("%v://%v/payapi/customer/source/remove?id=%v", c.proto, c.base, id), _json, nil, nil, responseNull, nil)
	return err
}
