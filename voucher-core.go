package pay

import (
	"time"
)

// Voucher represents data about a string key that may be used to apply discounts and similar
// behavioral changes to charges and subscriptions
type Voucher struct {
	Key       string    `json:"key"`
	CreatedAt time.Time `json:"createdAt"`
	UpdatedAt time.Time `json:"updatedAt"`
	// one off payments
	AmountOff  int64   `json:"amountOff"`
	PercentOff float64 `json:"percentOff"`
	// subscriptions
	TrialPeriod int64  `json:"trialPeriod"`
	Coupon      string `json:"coupon"`
	// limitations
	RemainingUses uint       `json:"remainingUses"`
	Expiry        *time.Time `json:"expiry"`
}
