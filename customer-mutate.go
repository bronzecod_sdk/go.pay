package pay

import (
	"fmt"
	"net/http"

	uuid "github.com/satori/go.uuid"
)

func (c *connection) CustomerNew(target uuid.UUID, name, email, phone, description, token string, metadata map[string]string) (Customer, error) {
	var res Customer
	d := marshal(map[string]interface{}{
		"for":         target,
		"name":        name,
		"email":       email,
		"phone":       phone,
		"description": description,
		"token":       token,
		"metadata":    metadata,
	})
	err := c.processRequest(http.MethodPost, fmt.Sprintf("%v://%v/payapi/customer/new", c.proto, c.base), _json, nil, d, responseJSON, &res)
	return res, err
}

func (c *connection) CustomerUpdateDetails(id uuid.UUID, name, email, phone, description string) error {
	d := marshal(map[string]interface{}{
		"customer":    id,
		"name":        name,
		"email":       email,
		"phone":       phone,
		"description": description,
	})
	err := c.processRequest(http.MethodPut, fmt.Sprintf("%v://%v/payapi/customer/update/details", c.proto, c.base), _json, nil, d, responseNull, nil)
	return err
}

func (c *connection) CustomerUpdateAddress(id uuid.UUID, isShipping bool, addr Address) error {
	args := map[string]interface{}{
		"customer": id,
		"type":     "billing",
		"line1":    addr.Line1,
		"line2":    addr.Line2,
		"city":     addr.City,
		"state":    addr.State,
		"postCode": addr.PostCode,
		"country":  addr.Country,
	}

	if isShipping {
		args["type"] = "shipping"
		args["name"] = addr.Name
		args["phone"] = addr.Phone
	}
	d := marshal(args)
	err := c.processRequest(http.MethodPut, fmt.Sprintf("%v://%v/payapi/customer/update/address", c.proto, c.base), _json, nil, d, responseNull, nil)
	return err
}

func (c *connection) CustomerDelete(id uuid.UUID) error {
	err := c.processRequest(http.MethodDelete, fmt.Sprintf("%v://%v/payapi/customer/delete?id=%v", c.proto, c.base, id), _json, nil, nil, responseNull, nil)
	return err
}
