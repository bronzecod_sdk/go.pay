package pay

import (
	"fmt"
	"net/http"
	"time"
)

func (c *connection) VoucherNewPayment(key string, percent float64, amount int64, useLimit uint, expiry *time.Time) (Voucher, error) {
	var res Voucher
	d := marshal(map[string]interface{}{
		"key":     key,
		"type":    "charge",
		"percent": percent,
		"amount":  amount,
		"limit":   useLimit,
		"expiry":  expiry,
	})
	err := c.processRequest(http.MethodPost, fmt.Sprintf("%v://%v/payapi/voucher/new", c.proto, c.base), _json, nil, d, responseJSON, &res)
	return res, err
}

func (c *connection) VoucherNewSubscription(key string, trial int64, stripeCoupon string, useLimit uint, expiry *time.Time) (Voucher, error) {
	var res Voucher
	d := marshal(map[string]interface{}{
		"key":         key,
		"type":        "subscription",
		"trialPeriod": trial,
		"coupon":      stripeCoupon,
		"limit":       useLimit,
		"expiry":      expiry,
	})
	err := c.processRequest(http.MethodPost, fmt.Sprintf("%v://%v/payapi/voucher/new", c.proto, c.base), _json, nil, d, responseJSON, &res)
	return res, err
}

func (c *connection) VoucherDelete(key string) error {
	err := c.processRequest(http.MethodDelete, fmt.Sprintf("%v://%v/payapi/voucher/delete?key=%v", c.proto, c.base, key), _json, nil, nil, responseNull, nil)
	return err
}
