package pay

import (
	"fmt"
	"net/http"
	"net/url"
	"strings"

	uuid "github.com/satori/go.uuid"
)

func (c *connection) CustomerPlatformList() ([]Customer, error) {
	var res []Customer
	err := c.processRequest(http.MethodGet, fmt.Sprintf("%v://%v/payapi/customers", c.proto, c.base), _json, nil, nil, responseJSON, &res)
	return res, err
}

func (c *connection) CustomerGet(id uuid.UUID, sources SourceMode) (Customer, error) {
	var res Customer
	err := c.processRequest(http.MethodGet, fmt.Sprintf("%v://%v/payapi/customer/get?id=%v&sources=%v", c.proto, c.base, id, sources), _json, nil, nil, responseJSON, &res)
	return res, err
}

func (c *connection) CustomerGetFor(target uuid.UUID, sources SourceMode) (Customer, error) {
	var res Customer
	err := c.processRequest(http.MethodGet, fmt.Sprintf("%v://%v/payapi/customer/get/for?target=%v&sources=%v", c.proto, c.base, target, sources), _json, nil, nil, responseJSON, &res)
	return res, err
}

func (c *connection) CustomerGetUserAccess(user uuid.UUID, sources SourceMode, queryCondition *QueryCondition) ([]Customer, error) {
	var res []Customer
	urlStr := fmt.Sprintf("%v://%v/payapi/customer/user/access?user=%v&sources=%v", c.proto, c.base, user, sources)
	if queryCondition != nil {
		if queryCondition.Condition != "" {
			urlStr += fmt.Sprintf("&condition=%v", url.QueryEscape(strings.Join([]string{queryCondition.Key, queryCondition.Condition}, "=")))
		} else {
			urlStr += fmt.Sprintf("&condition=%v", queryCondition)
		}
	}
	err := c.processRequest(http.MethodGet, urlStr, _json, nil, nil, responseJSON, &res)
	return res, err
}
