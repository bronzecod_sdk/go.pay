package pay

import (
	"fmt"
	"net/http"

	uuid "github.com/satori/go.uuid"
)

func (c *connection) SubscriptionGet(id uuid.UUID, lines LinesMode) (Subscription, error) {
	var res Subscription
	err := c.processRequest(http.MethodGet, fmt.Sprintf("%v://%v/payapi/subscription/get?id=%v&lines=%v", c.proto, c.base, id, lines), _json, nil, nil, responseJSON, &res)
	return res, err
}

func (c *connection) SubscriptionFor(target uuid.UUID, lines LinesMode) ([]Subscription, error) {
	var res []Subscription
	err := c.processRequest(http.MethodGet, fmt.Sprintf("%v://%v/payapi/subscription/for?id=%v&lines=%v", c.proto, c.base, target, lines), _json, nil, nil, responseJSON, &res)
	return res, err
}

func (c *connection) SubscriptionCustomer(customer uuid.UUID, lines LinesMode) ([]Subscription, error) {
	var res []Subscription
	err := c.processRequest(http.MethodGet, fmt.Sprintf("%v://%v/payapi/subscription/customer?id=%v&lines=%v", c.proto, c.base, customer, lines), _json, nil, nil, responseJSON, &res)
	return res, err
}
