// Package pay is a golang SDK for the Bronzecod payments REST API
//
// it provides type safe bindings for the APIs core features and entities
// as well as the various API endpoints available to get, enumerate and mutate them.
package pay
