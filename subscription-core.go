package pay

import (
	"time"

	uuid "github.com/satori/go.uuid"
)

// Subscription represents a recurring billing arrangement with a given customer
type Subscription struct {
	ID        uuid.UUID          `json:"id"`
	CreatedAt time.Time          `json:"createdAt"`
	UpdatedAt time.Time          `json:"updatedAt"`
	Customer  uuid.UUID          `json:"customer"`
	For       uuid.UUID          `json:"for"`
	Status    SubscriptionStatus `json:"status"`
	PeriodEnd time.Time          `json:"periodEnd"`
	TrialEnd  time.Time          `json:"trialEnd"`
	Metadata  map[string]string  `json:"metadata"`
	Lines     []LineItem         `json:"lines"`
}

// SubscriptionStatus represents the valid states a subscription may be in
type SubscriptionStatus string

const (
	// StatusIncomplete the subscription has failed its initial invoice or the invoice after ending a trial period
	StatusIncomplete SubscriptionStatus = "incomplete"
	// StatusIncompleteExpired the subscription failed its initial billing and was not resolved, it is now expired and cannot be recovered
	StatusIncompleteExpired SubscriptionStatus = "incomplete_expired"
	// StatusTrialing the subscription is currently in a trial period and not billing
	StatusTrialing SubscriptionStatus = "trialing"
	// StatusActive the subscription is active and billing normally
	StatusActive SubscriptionStatus = "active"
	// StatusPastDue the most recent charge for the invoice failed, or, if using manual invoices, the invoice is not paid
	StatusPastDue SubscriptionStatus = "past_due"
	// StatusCancelling the subscription is currently active, but has been marked to cancel at the end of the current billing period
	StatusCancelling SubscriptionStatus = "cancelling"
	// StatusCanceled the subscription has ended
	StatusCanceled SubscriptionStatus = "canceled"
	// StatusUnpaid the subscription is configured to enter the unpaid state rather than cancel when invoices fail to be paid in the configured number of attempts
	StatusUnpaid SubscriptionStatus = "unpaid"
)

// LineItem represents a single billing line on a subscription
//
// note: Plan field is a stripe price ID, it is named plan for legacy support
//
// note: NonRecurring being true indicates that line is an invoice item
// on the next invoice for the subscription and will not recur on subsequent invoices
type LineItem struct {
	ID           uuid.UUID `json:"id"`
	CreatedAt    time.Time `json:"createdat"`
	UpdatedAt    time.Time `json:"updatedat"`
	Subscription uuid.UUID `json:"subscription"`
	Plan         string    `json:"plan"`
	Quantity     int64     `json:"quantity"`
	Metered      bool      `json:"metered"`
	NonRecurring bool      `json:"nonRecurring"`
}

// LinesMode defines the behavior for loading line items
// for a subscription during retrieval
type LinesMode string

// the list of valid LinesMode constants
const (
	// LinesNone loads no line items, leaving only the core subscription details
	LinesNone LinesMode = "none"
	// LinesBase loads only recurring line items on the subscription
	LinesBase LinesMode = "base"
	// LinesAll loads all line items, both recurring, and pending one off invoice items
	LinesAll LinesMode = "all"
)

// InvoiceItem is a simplifies stripe.InvoiceItem used for transport
type InvoiceItem struct {
	ID string `json:"id"`
	Amount int64 `json:"amount"`
	Currency string `json:"currency"`
	Date int64 `json:"date"`
	Description string `json:"description"`
	Price struct {
		ID string `json:"id"`
	} `json:"price"`
	Proration bool `json:"proration"`
	Quantity int64 `json:"quantity"`
	UnitAmount int64 `json:"unit_amount"`
	Metadata map[string]string `json:"metadata"`
}
