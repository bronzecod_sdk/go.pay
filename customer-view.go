package pay

import (
	"fmt"
	"net/http"

	uuid "github.com/satori/go.uuid"
)

// CustomerView wraps the information to prepare a view of to
// collect/update customer details
type CustomerView struct {
	Customer    *uuid.UUID        `json:"customer"`
	For         *uuid.UUID        `json:"for"`
	Description *string           `json:"description"`
	Types       []string          `json:"types"`
	SingleUse   *bool             `json:"singleUse"`
	OnSession   *bool             `json:"onSession"`
	Metadata    map[string]string `json:"metadata"`
}

func (c *connection) CustomerViewPrepare(req CustomerView) (ViewDescriptor, error) {
	var res ViewDescriptor
	d := marshal(req)
	err := c.processRequest(http.MethodPost, fmt.Sprintf("%v://%v/payapi/details/prepare", c.proto, c.base), _json, nil, d, responseJSON, &res)
	return res, err
}

func (c *connection) CustomerViewResolve(token, name, phone, email string) (Customer, error) {
	var res Customer
	d := marshal(map[string]interface{}{
		"token": token,
		"name":  name,
		"email": email,
		"phone": phone,
	})
	err := c.processRequest(http.MethodPost, fmt.Sprintf("%v://%v/payapi/details/collect", c.proto, c.base), _json, nil, d, responseJSON, &res)
	return res, err
}
