package pay

import (
	"fmt"
	"net/http"

	uuid "github.com/satori/go.uuid"
)

func (c *connection) CustomerSourceGet(id uuid.UUID) (Source, error) {
	var res Source
	err := c.processRequest(http.MethodGet, fmt.Sprintf("%v://%v/payapi/customer/source/get?id=%v", c.proto, c.base, id), _json, nil, nil, responseJSON, &res)
	return res, err
}

func (c *connection) CustomerSourceDefault(customer uuid.UUID) (Source, error) {
	var res Source
	err := c.processRequest(http.MethodGet, fmt.Sprintf("%v://%v/payapi/customer/source/default?customer=%v", c.proto, c.base, customer), _json, nil, nil, responseJSON, &res)
	return res, err
}

func (c *connection) CustomerSourceList(customer uuid.UUID) ([]Source, error) {
	var res []Source
	err := c.processRequest(http.MethodGet, fmt.Sprintf("%v://%v/payapi/customer/source/list?customer=%v", c.proto, c.base, customer), _json, nil, nil, responseJSON, &res)
	return res, err
}
