package pay

import (
	"fmt"
	"net/http"

	uuid "github.com/satori/go.uuid"
)

func (c *connection) SubscriptionLineNew(subscription uuid.UUID, plan string, quantity int64, prorate bool) (LineItem, error) {
	var res LineItem
	d := marshal(map[string]interface{}{
		"subscription": subscription,
		"plan":         plan,
		"quantity":     quantity,
		"prorate":      prorate,
	})
	err := c.processRequest(http.MethodPost, fmt.Sprintf("%v://%v/payapi/subscription/line/new", c.proto, c.base), _json, nil, d, responseJSON, &res)
	return res, err
}

func (c *connection) SubscriptionLineUpdate(id uuid.UUID, quantity int64, delta, prorate bool) error {
	d := marshal(map[string]interface{}{
		"id":       id,
		"delta":    delta,
		"prorate":  prorate,
		"quantity": quantity,
	})
	err := c.processRequest(http.MethodPut, fmt.Sprintf("%v://%v/payapi/subscription/line/update", c.proto, c.base), _json, nil, d, responseNull, nil)
	return err
}

func (c *connection) SubscriptionLineDelete(id uuid.UUID) error {
	err := c.processRequest(http.MethodDelete, fmt.Sprintf("%v://%v/payapi/subscription/line/delete?id=%v", c.proto, c.base, id), _json, nil, nil, responseNull, nil)
	return err
}

func (c *connection) SubscriptionInvoiceExtraNewFromPlan(subscription uuid.UUID, plan string, quantity int64) (InvoiceItem, error) {
	var res InvoiceItem
	d := marshal(map[string]interface{}{
		"subscription": subscription,
		"plan":         plan,
		"quantity":     quantity,
	})
	err := c.processRequest(http.MethodPost, fmt.Sprintf("%v://%v/payapi/subscription/extra/new", c.proto, c.base), _json, nil, d, responseJSON, &res)
	return res, err
}

func (c *connection) SubscriptionInvoiceExtraNewWithDetails(subscription uuid.UUID, unitPrice int64, quantity int64, title, description string) (InvoiceItem, error) {
	var res InvoiceItem
	d := marshal(map[string]interface{}{
		"subscription": subscription,
		"quantity":     quantity,
		"priceData": map[string]interface{}{
			"title":       title,
			"description": description,
			"price":       unitPrice,
		},
	})
	err := c.processRequest(http.MethodPost, fmt.Sprintf("%v://%v/payapi/subscription/extra/new", c.proto, c.base), _json, nil, d, responseJSON, &res)
	return res, err
}

func (c *connection) SubscriptionInvoiceExtraDelete(key string) error {
	err := c.processRequest(http.MethodDelete, fmt.Sprintf("%v://%v/payapi/subscription/extra/delete?key=%v", c.proto, c.base, key), _json, nil, nil, responseNull, nil)
	return err
}
