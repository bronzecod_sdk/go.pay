package pay

import (
	"fmt"
	"net/http"

	uuid "github.com/satori/go.uuid"
)

func (c *connection) SubscriptionGetLines(subscription uuid.UUID, lines LinesMode) ([]LineItem, error) {
	var res []LineItem
	err := c.processRequest(http.MethodGet, fmt.Sprintf("%v://%v/payapi/subscription/get/lines?id=%v&lines=%v", c.proto, c.base, subscription, lines), _json, nil, nil, responseJSON, &res)
	return res, err
}
