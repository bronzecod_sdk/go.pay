package pay

import (
	"time"

	uuid "github.com/satori/go.uuid"
)

// Connection is an instance of a connection to the bronzecod Payments server
// each function corresponds to a synchronous API call to the server
type Connection interface {
	// =====================
	//   charges interface
	// =====================

	// Charge will create and optionally resolve the described charge, either billing the identified customer, or
	// creating a pending collect request that may be resolved later
	Charge(req ChargeRequest) (string, error)
	// ChargeCapture will resolve the identified pending capture, charging the identified amount and closing the request
	ChargeCapture(id string, amount int64) error

	// =====================
	//  customers interface
	// =====================

	// customers get

	// CustomerPlatformList returns a summary list of all customers available on the platform for the requesting account
	CustomerPlatformList() ([]Customer, error)
	// CustomerGet retrieves the customer with the corresponding ID, and optionally their billing sources based on the provided source mode
	CustomerGet(id uuid.UUID, sources SourceMode) (Customer, error)
	// CustomerGetFor retrieves the customer with the corresponding for identifier, and optionally their billing sources based on the provided source mode
	CustomerGetFor(target uuid.UUID, sources SourceMode) (Customer, error)
	// CustomerGetUserAccess retrieves all customers a user has access permissions to, and optionally their billing sources based on the provided source mode
	CustomerGetUserAccess(user uuid.UUID, sources SourceMode, queryCondition *QueryCondition) ([]Customer, error)

	// customers mutate

	// CustomerNew creates a new customer with the provided details for the "for" target identifier, name, email, phone, description, payment method token and metadata
	CustomerNew(target uuid.UUID, name, email, phone, description, token string, metadata map[string]string) (Customer, error)
	// CustomerUpdateDetails updates the identified customers labelling details (name, email, phone and description)
	CustomerUpdateDetails(id uuid.UUID, name, email, phone, description string) error
	// CustomerUpdateAddress updates a customers address details, either their billing or shipping address
	CustomerUpdateAddress(id uuid.UUID, isShipping bool, addr Address) error
	// CustomerDelete deletes the identified customer
	CustomerDelete(id uuid.UUID) error

	// sources get

	// CustomerSourceGet gets the "current" source of the identified customer, or their most recent if none are set as "current"
	CustomerSourceGet(id uuid.UUID) (Source, error)
	// CustomerSourceDefault gets the "default source of the identified customer, or the most recent if none are set as "default"
	CustomerSourceDefault(customer uuid.UUID) (Source, error)
	// CustomerSourceList lists all valid payment sources on a customer
	CustomerSourceList(customer uuid.UUID) ([]Source, error)

	// sources mutate

	// CustomerSourceAdd adds a tokenized payment method to the identified customer
	CustomerSourceAdd(customer uuid.UUID, method string) (Source, error)
	// CustomerSourceRemove removes a stored payment method from its customer, and the platform
	CustomerSourceRemove(id uuid.UUID) error

	// =========================
	//  subscriptions interface
	// =========================

	// subscriptions get

	// SubscriptionGet retrieves the subscription identified, with the line items populated according to the provided lines mode
	SubscriptionGet(id uuid.UUID, lines LinesMode) (Subscription, error)
	// SubscriptionFor retrieves the list of subscriptions for a given target, with the line items populated according to the provided lines mode
	SubscriptionFor(target uuid.UUID, lines LinesMode) ([]Subscription, error)
	// SubscriptionCustomer retrieves all subscriptions a customer pays, with the line items populated according to the provided lines mode
	SubscriptionCustomer(customer uuid.UUID, lines LinesMode) ([]Subscription, error)

	// subscriptions mutate

	// SubscriptionNew creates a new subscription with the given details for paying customer, target product/permission, line items and setup costs
	SubscriptionNew(customer uuid.UUID, target uuid.UUID, lines, setup []SubscriptionNewPlan, metadata map[string]string, voucher string) (Subscription, error)
	// SubscriptionUpdateMetadata updates the identified subscriptions metadata with the provided values, either replacing the existing metadata or merging them based on the flag
	SubscriptionUpdateMetadata(id uuid.UUID, metadata map[string]string, merge bool) error
	// SubscriptionDelete deletes the identified subscription
	SubscriptionDelete(id uuid.UUID) error

	// subscription lines get

	// SubscriptionGetLines retrieves line items for a subscription subject to the lines mode provided
	SubscriptionGetLines(subscription uuid.UUID, lines LinesMode) ([]LineItem, error)

	// subscription lines mutate

	// SubscriptionLineNew creates a new line item in the identified subscription with the given plan key, quantity and whether to prorate billing or bill for the whole period
	SubscriptionLineNew(subscription uuid.UUID, plan string, quantity int64, prorate bool) (LineItem, error)
	// SubscriptionLineUpdate updates a subscription line item, changing it quantity, notating if the quantity is a change or a delta, and if the change should be prorated
	SubscriptionLineUpdate(id uuid.UUID, quantity int64, delta, prorate bool) error
	// SubscriptionLineDelete deletes the identified line item from its subscription
	SubscriptionLineDelete(id uuid.UUID) error
	// SubscriptionInvoiceExtraNewFromPlan adds an invoice item to the identified subscription that will appear on the next invoice but not recur. Uses the identified plan key and quantity
	SubscriptionInvoiceExtraNewFromPlan(subscription uuid.UUID, plan string, quantity int64) (InvoiceItem, error)
	// SubscriptionInvoiceExtraNewWithDetails adds an invoice item to the identified subscription, providing specific details for the line item such as quantity, unit price, a title and description.
	SubscriptionInvoiceExtraNewWithDetails(subscription uuid.UUID, unitPrice int64, quantity int64, title, description string) (InvoiceItem, error)
	// SubscriptionInvoiceExtraDelete removes an un-charged invoice item from the next invoice of its subscription
	SubscriptionInvoiceExtraDelete(key string) error

	// =====================
	//   vouchers interface
	// =====================

	// vouchers get

	// VoucherExists checks if the provided voucher key exists and is currently valid
	VoucherExists(key string) (exists bool, valid bool, err error)
	// VoucherGet retrieves the voucher with the provided key
	VoucherGet(key string) (Voucher, error)
	// VoucherList lists all vouchers on the current platform (optionally filtered to only those currently valid)
	VoucherList(activeOnly bool) ([]Voucher, error)

	// vouchers mutate

	// VoucherNewPayment creates a new voucher that can be used to discount payments (charges) with the provided details
	VoucherNewPayment(key string, percent float64, amount int64, useLimit uint, expiry *time.Time) (Voucher, error)
	// VoucherNewSubscription creates a new voucher that can be used to discount subscriptions recurrently, with the provided details
	VoucherNewSubscription(key string, trial int64, stripeCoupon string, useLimit uint, expiry *time.Time) (Voucher, error)
	// VoucherDelete deletes the identified voucher from the platform
	VoucherDelete(key string) error

	// =====================
	//   views preparation
	// =====================

	// ChargeViewPrepare creates a view url to charge a customer with the provided details
	ChargeViewPrepare(req ChargeView) (ViewDescriptor, error)
	// CustomerViewPrepare creates a view url to display a customers billing details, or collect the details of a new customer
	CustomerViewPrepare(req CustomerView) (ViewDescriptor, error)
	// CustomerViewResolve resolves the collected details from a customer details collect view (CustomerViewPrepare) and uses the resultant token to create a customer with the provided details
	CustomerViewResolve(token, name, phone, email string) (Customer, error)
	// SubscriptionViewPrepare creates a view url to view, create or update a subscription with the provided details
	SubscriptionViewPrepare(req SubscriptionView) (ViewDescriptor, error)
}
