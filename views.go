package pay

// ViewDescriptor describes a token used to create a payment server view
// 
// Token is the ID of the prepared view and its transaction,
// and where appropriate can be used to complete the transaction with
// subsequent server side step.
//
// URL is the address a user should be redirected to in order to view the prepared page view
type ViewDescriptor struct {
	Token string `json:"token"`
	URL   string `json:"url"`
}
