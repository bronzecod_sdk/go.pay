package pay

import (
	"fmt"
	"net/http"

	uuid "github.com/satori/go.uuid"
)

// SubscriptionView wraps the information to prepare a view to
// create/update a subscription
type SubscriptionView struct {
	Subscription *uuid.UUID            `json:"subscription"`
	For          *uuid.UUID            `json:"for"`
	Customer     *uuid.UUID            `json:"customer"`
	CustomerFor  *uuid.UUID            `json:"customerFor"`
	Plans        []SubscriptionNewPlan `json:"plans"`
	Setup        []SubscriptionNewPlan `json:"setup"`
	Metadata     map[string]string     `json:"metadata"`
	Prorate      bool                  `json:"prorate"`
}

func (c *connection) SubscriptionViewPrepare(req SubscriptionView) (ViewDescriptor, error) {
	var res ViewDescriptor
	d := marshal(req)
	err := c.processRequest(http.MethodPost, fmt.Sprintf("%v://%v/payapi/subscription/prepare", c.proto, c.base), _json, nil, d, responseJSON, &res)
	return res, err
}
