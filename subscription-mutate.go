package pay

import (
    "fmt"
    "net/http"

    uuid "github.com/satori/go.uuid"
)

// SubscriptionNewPlan wraps a pair of price/plan keys and the quantity of that plan
type SubscriptionNewPlan struct {
    Plan     string `json:"plan"`
    Quantity int64  `json:"quantity"`
}

func (c *connection) SubscriptionNew(customer uuid.UUID, target uuid.UUID, lines, setup []SubscriptionNewPlan, metadata map[string]string, voucher string) (Subscription, error) {
    var res Subscription
    d := marshal(map[string]interface{}{
        "customer": customer,
        "for":      target,
        "lines":    lines,
        "setup":    setup,
        "metadata": metadata,
        "voucher":  voucher,
    })
    err := c.processRequest(http.MethodPost, fmt.Sprintf("%v://%v/payapi/subscription/new", c.proto, c.base), _json, nil, d, responseJSON, &res)
    return res, err
}

func (c *connection) SubscriptionUpdateMetadata(id uuid.UUID, metadata map[string]string, merge bool) error {
    d := marshal(map[string]interface{}{
        "id":       id,
        "metadata": metadata,
    })
    err := c.processRequest(http.MethodPut, fmt.Sprintf("%v://%v/payapi/subscription/update?merge=%v", c.proto, c.base, merge), _json, nil, d, responseNull, nil)
    return err
}

func (c *connection) SubscriptionDelete(id uuid.UUID) error {
    err := c.processRequest(http.MethodDelete, fmt.Sprintf("%v://%v/payapi/subscription/delete?id=%v", c.proto, c.base, id), _json, nil, nil, responseNull, nil)
    return err
}
