package pay

import (
	"fmt"
	"net/http"
)

func (c *connection) VoucherExists(key string) (exists bool, valid bool, err error) {
	var res struct {
		Exists bool `json:"exists"`
		Valid  bool `json:"valid"`
	}
	err = c.processRequest(http.MethodGet, fmt.Sprintf("%v://%v/payapi/voucher/exists?key=%v", c.proto, c.base, key), _json, nil, nil, responseJSON, &res)
	return res.Exists, res.Valid, err
}

func (c *connection) VoucherGet(key string) (Voucher, error) {
	var res Voucher
	err := c.processRequest(http.MethodGet, fmt.Sprintf("%v://%v/payapi/voucher/get?key=%v", c.proto, c.base, key), _json, nil, nil, responseJSON, &res)
	return res, err
}

func (c *connection) VoucherList(activeOnly bool) ([]Voucher, error) {
	var res []Voucher
	err := c.processRequest(http.MethodGet, fmt.Sprintf("%v://%v/payapi/voucher/list?active=%v", c.proto, c.base, activeOnly), _json, nil, nil, responseJSON, &res)
	return res, err
}
