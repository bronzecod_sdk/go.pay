package pay

import (
	"time"

	uuid "github.com/satori/go.uuid"
)

// Customer represents a recorded recurring customer
// collecting general details of the customer, eternal references
// an internal balance, and a collection of payment sources
type Customer struct {
	ID          uuid.UUID         `json:"id"`
	CreatedAt   time.Time         `json:"createdAt"`
	UpdatedAt   time.Time         `json:"updatedAt"`
	For         uuid.UUID         `json:"for"`
	Name        string            `json:"name,omitempty"`
	Email       string            `json:"email"`
	Phone       string            `json:"phone"`
	Description string            `json:"description"`
	Currency    string            `json:"currency"`
	Balance     int64             `json:"balance"`
	MetaData    map[string]string `json:"metaData"`
	Address     *Address          `json:"Address,omitempty"`
	Shipping    *Address          `json:"Shipping,omitempty"`
	Sources     []Source          `json:"sources,omitempty"`
}

// SourceMode defines which of a users source list to retrieve
type SourceMode string

const (
	// SourceNone gets no sources for a customer
	SourceNone SourceMode = "none"
	// SourceCurrent gets only the current "default" or most recent valid source
	SourceCurrent SourceMode = "current"
	// SourceAll gets all a customers sources that are current and valid
	SourceAll SourceMode = "all"
)

// Source is a payment method a customer may use to pay for a transaction
type Source struct {
	ID         uuid.UUID         `json:"id"`
	CreatedAt  time.Time         `json:"createdAt"`
	UpdatedAt  time.Time         `json:"updatedAt"`
	Customer   uuid.UUID         `json:"customer"`
	Name       string            `json:"name"`
	Source     string            `json:"source"`
	Identifier string            `json:"identifier"`
	Expiry     *time.Time        `json:"expiry,omitempty"`
	Address    *Address          `json:"address,omitempty"`
	Metadata   map[string]string `json:"metadata"`
}

// Address collects the details of either a billing or shipping address
//
// note: name and phone should never be populated for a billing address
type Address struct {
	Name     string `json:"recipient,omitempty"`
	Phone    string `json:"contact,omitempty"`
	Line1    string `json:"line1"`
	Line2    string `json:"line2,omitempty"`
	City     string `json:"city"`
	State    string `json:"state"`
	PostCode string `json:"postCode"`
	Country  string `json:"country"`
}

// QueryCondition is a wrapper type used to imply conditions when querying a list of customers for a user
// allowing constraints on required permission to that customer object by key and a regular expression condition
type QueryCondition struct {
	Key       string
	Condition string
}
