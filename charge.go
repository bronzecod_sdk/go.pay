package pay

import (
	"fmt"
	"net/http"

	uuid "github.com/satori/go.uuid"
)

// ChargeRequest handles a request to charge a user while offline
// Either a customer ID or a stripe payment method ID must be provided
//
// setting collect to false will create the charge but not capture and settle the transaction
// in this case the returned transaction ID may be used in the ChargeCapture endpoint to collect the funds
type ChargeRequest struct {
	Customer    *uuid.UUID        `json:"customer,omitempty"`
	Method      string            `json:"method,omitempty"`
	Amount      int64             `json:"amount"`
	Collect     *bool             `json:"collect"`
	Currency    string            `json:"currency"`
	Description string            `json:"description"`
	Statement   string            `json:"statement"`
	Voucher     string            `json:"voucher"`
	Metadata    map[string]string `json:"metadata"`
}

func (c *connection) Charge(req ChargeRequest) (string, error) {
	var res string
	d := marshal(req)
	err := c.processRequest(http.MethodPost, fmt.Sprintf("%v://%v/payapi/charge", c.proto, c.base), _json, nil, d, responseString, &res)
	return res, err
}

func (c *connection) ChargeCapture(id string, amount int64) error {
	d := marshal(map[string]interface{}{
		"id":     id,
		"amount": amount,
	})
	err := c.processRequest(http.MethodPost, fmt.Sprintf("%v://%v/payapi/charge/capture", c.proto, c.base), _json, nil, d, responseNull, nil)
	return err
}
