package pay

import (
	"fmt"
	"net/http"

	uuid "github.com/satori/go.uuid"
)

// ChargeView wraps the information to prepare a view of a
// charge for a user
type ChargeView struct {
	Currency            string            `json:"currency"`
	Amount              int64             `json:"amount"`
	Description         *string           `json:"description"`
	StatementDescriptor *string           `json:"statementDescriptor"`
	For                 *uuid.UUID        `json:"for"`
	Customer            *uuid.UUID        `json:"customer"`
	Method              *string           `json:"method"`
	UseOffSession       *bool             `json:"usage"`
	Collect             *bool             `json:"collect"`
	Details             *bool             `json:"details"`
	Shipping            *bool             `json:"shipping"`
	AllowSave           *bool             `json:"allowSave"`
	AllowSignup         *bool             `json:"allowSignup"`
	Metadata            map[string]string `json:"metadata"`
}

func (c *connection) ChargeViewPrepare(req ChargeView) (ViewDescriptor, error) {
	var res ViewDescriptor
	d := marshal(req)
	err := c.processRequest(http.MethodPost, fmt.Sprintf("%v://%v/payapi/charge/prepare", c.proto, c.base), _json, nil, d, responseJSON, &res)
	return res, err
}
