package pay

import (
	"time"

	uuid "github.com/satori/go.uuid"
)

// String utility to create String pointers
func String(a string) *string {
	return &a
}

// Bool utility to create Bool pointers
func Bool(a bool) *bool {
	return &a
}

// UUID utility to create UUID pointers
func UUID(a uuid.UUID) *uuid.UUID {
	return &a
}

// Time utility to create Time pointers
func Time(a time.Time) *time.Time {
	return &a
}
